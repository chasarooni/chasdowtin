let header = document.querySelector('#intro');
let anim = [
    { t: "{ }", ms: 200 },
    { t: "{_}", ms: 200 },
    { t: "{ }", ms: 200 },
    { t: "{_}", ms: 200 },
    { t: "{C_}", ms: 100 },
    { t: "{CH_}", ms: 100 },
    { t: "{CHA_}", ms: 100 },
    { t: "{CHAS_}", ms: 100 },
    { t: "{CHAS__}", ms: 100 },
    { t: "{CHAS_D_}", ms: 100 },
    { t: "{CHAS_DO_}", ms: 100 },
    { t: "{CHAS_DOW_}", ms: 100 },
    { t: "{CHAS_DOWT_}", ms: 100 },
    { t: "{CHAS_DOWTIN}", ms: 200 },
    { t: "{CHAS_DOWTIN }", ms: 200 },
    { t: "{CHAS_DOWTIN_}", ms: 200 },
    { t: "{CHAS_DOWTIN }", ms: 200 },
    { t: "{CHAS_DOWTIN_}", ms: 200 },
    { t: "{CHAS_DOWTIN}", ms: 200 },
    { t: "{CHAS_DOWTIN}", ms: 200 }
];
let stepDenominator = 1;
if (window.localStorage.stepDenominator)
    stepDenominator = window.localStorage.stepDenominator;
let i = 0;
let update = () => {
    let step = anim[i];
    header.innerText = step.t;
    i++;

    if (i < anim.length)
        setTimeout(update, step.ms / stepDenominator);
    else {
        header.classList.add('top');
        setTimeout(() => {
            document.getElementById('main').style.opacity = 1;
            initGlobe();
        }, 500);
        window.localStorage.stepDenominator = 2;
    }
}
update();